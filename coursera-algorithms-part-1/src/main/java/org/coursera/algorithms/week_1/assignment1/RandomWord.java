package org.coursera.algorithms.week_1.assignment1;

import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomWord {
    public static void main(String[] args) {
        String champion = "";
        int count = 0;

        while (StdIn.isEmpty()) {
            count++;
            String challenger = StdIn.readString();
            if (StdRandom.bernoulli(1.0 / count)) {
                champion = challenger;
            }
        }

        StdOut.println(champion);
    }

}
